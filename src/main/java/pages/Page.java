package pages;

import core.utils.BrowerConfig.WebConnector;
import core.utils.ConfigUtil;
import io.cucumber.java8.Pa;
import org.openqa.selenium.WebDriver;
import pages.android.*;

import java.util.Properties;

public class Page {

    public static Properties myProp = ConfigUtil.getConfig("config");
    public WebDriver driver = WebConnector.driver;

    //Android
    PageLogin pageLogin = new PageLogin(this);
    protected PageLogin getLoginPage() {return pageLogin;}

}
