package pages.android;

import core.utils.AndroidCore.AndroidDriverSetup;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.Page;

import static core.utils.AppiumHook.AppiumServerRunner.driver;

public class PageLogin {

    private String ENTER_IP_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.EditText";
    private String OKAY_BUTTON_ID = "android:id/button1";
    private String ALLOW_DEVICE_LOCATION_ID = "com.android.packageinstaller:id/permission_allow_button";
    private String DENY_DEVICE_LOCATION_ID = "com.android.packageinstaller:id/permission_deny_button";
    private String ALLOW_PHONE_CALLS_ID = "com.android.packageinstaller:id/permission_allow_button";
    private String DENY_PHONE_CALLS_ID = "com.android.packageinstaller:id/permission_deny_button";
    private String ENGLISH_RADIO_BUTTON_ID = "com.bykea.pk:id/tvEnglish";
    private String URDU_RADIO_BUTTON_ID = "com.bykea.pk:id/rbUrdu";
    private String TICK_BUTTON_ID = "com.bykea.pk:id/ivPositive";
    private String INSERT_MOBILENUMBER_ID = "com.bykea.pk:id/phoneNumberEt";
    private String LOGIN_BUTTON_ID = "com.bykea.pk:id/loginBtn";
    private String ENTER_OTP_ID = "com.bykea.pk:id/verificationCodeEt_1";
    private String RESEND_OTP_ID = "com.bykea.pk:id/resendTv";

    public PageLogin(Page page) {
    }

    public WebElement enterIp() {return AndroidDriverSetup.getAndroidDriver().findElement(By.xpath(ENTER_IP_XPATH));}
    public WebElement getOkayButton() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(OKAY_BUTTON_ID));}
    public WebElement getAllowDeviceLocation() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(ALLOW_DEVICE_LOCATION_ID));}
    public WebElement getDenyDeviceLocation() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(DENY_DEVICE_LOCATION_ID));}
    public WebElement getAllowPhoneCalls() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(ALLOW_PHONE_CALLS_ID));}
    public WebElement getDenyPhoneCalls() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(DENY_PHONE_CALLS_ID));}
    public WebElement getEnglishRadioButton() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(ENGLISH_RADIO_BUTTON_ID));}
    public WebElement getUrduRadioButton() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(URDU_RADIO_BUTTON_ID));}
    public WebElement getTickButton() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(TICK_BUTTON_ID));}
    public WebElement insertMobileNumber() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(INSERT_MOBILENUMBER_ID));}
    public WebElement getLoginButton() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(LOGIN_BUTTON_ID));}
    public WebElement enterOtp() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(ENTER_OTP_ID));}
    public WebElement resendOtp() {return AndroidDriverSetup.getAndroidDriver().findElement(By.id(RESEND_OTP_ID));}
}
