package stepDefination.Android;

import io.cucumber.java.en.*;
import pages.Page;
import java.io.IOException;

public class LoginPage extends Page {

    @Given("[Login Page]App install successfully")
    public void loginPageAppInstallSuccessfully() {
        System.out.println("Open Bykea App");
    }


//    @Given("[Login Page]App install successfully")
//   public void openAndroidApp() throws IOException, InterruptedException {
//        System.out.println("Open Bykea App");
//   }

   @When("[Login Page]User enters correct talos url")
   public  void enterIp() throws IOException, InterruptedException {
        getLoginPage().enterIp().click();
        getLoginPage().enterIp().clear();
        getLoginPage().enterIp().sendKeys("https://ecommerce2-talos.bykea.dev");
        getLoginPage().getOkayButton().click();
    }

    @And("[Login Page]User allow device location")
    public  void getAllowDeviceLocation() throws IOException,InterruptedException {
        getLoginPage().getAllowDeviceLocation().click();
    }

   @And("[Login Page]User make and manage phone calls")
    public  void getAllowPhoneCalls() throws IOException,InterruptedException {
        getLoginPage().getDenyPhoneCalls().click();
    }

    @And("[Login Page]User selects app language")
    public  void getEnglishRadioButton() throws IOException,InterruptedException {
        getLoginPage().getUrduRadioButton().click();
        getLoginPage().getTickButton().click();
    }

    @And("[Login Page]User enter correct mobilenumber")
    public  void insertMobileNumber() throws IOException,InterruptedException {
        getLoginPage().insertMobileNumber().sendKeys("03322971040");
        getLoginPage().getLoginButton().click();
    }

    @And("[Login Page]User enter correct otp")
    public  void enterOtp() throws IOException,InterruptedException {
        getLoginPage().enterOtp().sendKeys("0000");
    }

    @Then("[Login Page]User successfully login to Bykea Home Page")
    public  void bykeaHomePage() throws IOException,InterruptedException {
        System.out.println("User Logged in Successfully.");
    }

}