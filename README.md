**Bykea Framework :**

1- Android \
2- Web \
3- iOS 


**SOFTWARE INSTALLATION:**

You need to install following software:

- JAVA jdk
- IntelliJ
- Chrome Driver
- Node JS
- Android Sdk
- Appium Server


**Download JDK**

Download From https://www.oracle.com/java/technologies/downloads/

Install the JDK on your machine

Also, set Environment Variables 

**Download ANDROID SDK**

Download sdk  From https://developer.android.com/studio

Set ANDROID_HOME as environment variable by adding mentioned path in .bashrc file

          - export ANDROID_HOME=/home/user/Android/Sdk
 
Also set path variables by following by adding :

         - export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

**Appium Installation:**

You can follow following link for installation https://medium.com/@syamsasi/setting-up-appium-on-windows-and-ubuntu-ea9a73ab989
 
**IntelliJ Installation:**

Download the intellij from their official website.

Install the intellij on your machine.
 
**Repository Checkout**

Open the  terminal and clone the project 
           
        - git clone http://artifactory.devcrud.uk/syedzubair/bykea-automation.git

After cloning the repository, go the cloned folder and fetch the repository
          
        -  git fetch
Now checkout the branch
        
        -  git checkout <branch_name>

And in last pull the branch by following command

        - git pull
 
 





