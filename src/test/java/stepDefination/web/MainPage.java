package stepDefination.web;


import core.utils.BrowerConfig.WebConnector;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import pages.Page;
import static core.utils.AndroidCore.AndroidDriverSetup.quitAndroidDriver;
import static core.utils.AndroidCore.AppiumServerRunner.StartAppiumServer;
import static core.utils.AndroidCore.AndroidDriverSetup.androidDriver;
import static stepDefination.Android.AndroidSetup.*;

public class MainPage extends Page {

    @Before("@Web")
    public void startTest() throws Exception {
        if (myProp.getProperty("platformname").toString().equals("Web")) {
            WebConnector connector = new WebConnector();
            driver = connector.openBrowser();
            driver.get(myProp.getProperty("weburl"));
            driver.manage().window().maximize();
        }
    }

    @Before("@initial")
    public void startTestAndroid() throws Exception {
        if (myProp.getProperty("platformname").toString().equals("Android")) {
            PORT = myProp.getProperty("appiumport");
            StartAppiumServer(PORT);
            androidDriver(PORT);
        }
    }

    @After("@Web")
    public void afterTest() {
        if (myProp.getProperty("platformname").toString().equals("Web")) {
            if (driver != null) {
                driver.quit();
            }
        }
    }

    @After("@final")
    public void afterTestAndroid() {
        if (myProp.getProperty("platformname").toString().equals("Android")) {
            quitAndroidDriver();
        }
    }

    public void iOSDriver() {
    }


    @Given("^\\[Main Page] Navigate to the bykea home page$")
    public void  openWebUrl() {
        String webUrl = myProp.getProperty("weburl");
        driver.get(webUrl);
        driver.manage().window().maximize();
    }

    @When("^\\[Main Page] Navigate to Contact bykea page$")
    public void openContactPage() throws InterruptedException {

    }

    @And("^\\[Main Page] Form fields are filled$")
    public void fillContactForm() throws InterruptedException {
    }


}
